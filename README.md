# YapokWeb
## Autores
- Andrés Perdiguero
- Sergio Belkin

## Trayecto:

- Administración
- Educación

## Director

- Alejandro López

## Resumen

YapokWeb es una distribución de Linux orientada a Programación Web.

## Justificación

Nos propusimos realizar una distribución de Linux con herramientas útiles para programar web:
- Frameworks
- IDE's
- Editores

**No es una mera acumulación de herramientas**

Queremos que YapokWeb sea un recurso interesante para aquellas personas que deseen contar con un sistema operativo orientado a uno de los lenguajes de programación más requeridos en el mercado laboral argentino. Nuestro propósito es propiciar que otras personas puedan continuar el proyecto. Por ello hemos decidido documentar el proceso de armado de la distribución.

Al principio queríamos hacer una distribución más ambiciosa, pero el objetivo era demasiado amplio. Decidimos acotar nuestro alcance.

Digamos que nuestro TPF tiene dos objetivos sencillos pero específicos: uno enfocado al usuario y otro al desarrollador y/o colaborador de un proyecto de software libre.

Ejemplo: https://www.educacionit.com/empleos-stats

## Estado del arte

**¿Cuál el propósito principal de Yapok Web?**

Crear una distribución adaptada para Argentina con herramientas para  practicar el desarrollo web con software libre y Linux, focalizada en particular en JAVA. Además, documentar distintos aspectos de armado de una distribución: Entorno aislado, selección de paquetes, configuración de entorno predeterminado, creación de la iso, configuraciones predeterminadas. Nos interesa documentar este proceso.

**¿Existen otros programas que hacen lo mismo?**
Existe muchos proyectos relacionados con formación y/o educación
    • Edubuntu
    • Fedora Python Classroom
    • Skolelinux
    • Modicia O.S.
    • PrimTux
    
Nuestro objetivo sin embargo, apunta al público en general adulto.
 
 Sin embargo exceptuando Python Classroom de Fedora no hay distribuciones orientadas específicamente a programación. 
 
 Respecto al armado de una distribución existe Linux From Scratch, pero estamos interesados en un proceso bastante más abreviado que saque provecho de software existente sin tener que reinventar la rueda.
 
¿Cuál es la ventaja sobre los anteriores?
    • Edubuntu: La última versión es de 2015.
    • Fedora Python Classroom: Está focalizada en el aprendizaje de Python, pero no hace foco en el desarrollo web.
    • Skolelinux: La última versión es de 2016.
    • Modicia O.S. : Es una distribución italiana, tiene una versión para escuelas pero no está enfocada en el aprendizaje de programación web
    • PrimTux: Es una distribución francesa  y tampoco está orientada a aprendizaje de programación web

 **Queremos hacer hincapié en que si bien hacemos esta distribución con la intención de que el usuario puede entrenarse en la programación no creemos que un software determinado garantice al aprendizaje de una determinada tecnología**

Queremos además dejar documentado como se puede agregar software (existente) o software desarrollado específicamente para ampliar el alcance de la distribución tanto para quienes se quieran capacitar en programación como para facilitadores.

¿En qué proyecto nos basaríamos?

Nos basaríamos en Fedora. Lla razón principal más allá de que esté pensada para aprender programación es que Fedora posee un herramientas idóneas para remasterizar imágenes ISOS.

Queremos enfatizar que existen distribuciones para Seguridad, de Rescate, Forense, etc. pero no existe una para desarrollo. 

## LICENCIA de la documentación

Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional.

